"My vim settings taken from different resources:
"    https://realpython.com/vim-and-python-a-match-made-in-heaven/#verifying-your-vim-install
"    https://github.com/j1z0/vim-config/blob/master/vimrc
"    https://github.com/amix/vimrc
"    https://gitlab.com/LukeSmithxyz/voidrice/-/blob/master/.config/nvim/init.vim
"    https://github.com/lawrencesystems/dotfiles/blob/master/.vimrc
"    https://github.com/changemewtf/dotfiles/tree/master/vim
"    https://github.com/changemewtf/no_plugins
"    https://stackoverflow.com/questions/2600783/how-does-the-vim-write-with-sudo-trick-work


" Read documentation about each option by executing :h <option>


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Sections:
"    -> General
"    -> Plugins
"    -> VIM user interface
"    -> Colors and Fonts
"    -> Files and backups
"    -> Text, tab and indent related
"    -> Visual mode related
"    -> Moving around, tabs and buffers
"    -> Status line
"    -> Editing mappings
"    -> Spell checking
"    -> Miscellaneous
"    -> Helper functions
"
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""



"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => General
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Do not preserve compatibility with Vi
        set nocompatible

" Disable file type detection. It is required in order to load plugins
        filetype off

" Sets how many lines of history VIM has to remember
        set history=1000

" Detect when a file has been modified externally
        set autoread
        au FocusGained,BufEnter * checktime

" With a map leader it's possible to do extra key combinations
" like <leader>w saves the current file
        let mapleader = ","

" Easy saving
        inoremap <C-u> <ESC>:w<CR>

" Fast saving
        nmap <leader>w :w!<CR>

" :W saves file as sudo on files that require root permission
" (useful for handling the permission-denied error)
        command! W execute 'w !sudo tee % > /dev/null' <bar> edit!

" Access to the system clipboard
        set clipboard+=unnamedplus

" Modelines
        set modelines=2
        set modeline

" For clever completion with the :find command
        set path+=**

" Set the file name at the window frame
        set title

" Quickly open buffer list to select one by number or name
" Usage:
"    gb
"    (quickly scanning the list)
"    3<CR>
"
"    or:
"
"    gb
"    (quickly scanning the list)
"    foo<tab><CR>
        nnoremap gb :ls<CR>:b<Space>



"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Plugins
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"NONE SO FAR

" Enable filetype plugins
        filetype plugin on
        filetype indent on

" Disables automatic commenting on newline, see <:help fo-table>
        autocmd FileType * setlocal formatoptions-=r formatoptions-=o



"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Vim user interface
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Default character encoding
        set encoding=utf-8

" Languages to check for spelling (english, greek)
        set spelllang=en,es

" Number of suggestions for correct spelling
        set spellsuggest=10

" How many undo levels to keep in memory
        set undolevels=1000

" Enable tab completion with suggestions when executing commands
        set wildmenu

" Settings for how to complete matched strings
        set wildmode=list,longest,full

" Always show current position
        set ruler

" Ignore case when searching
        set ignorecase

" When searching try to be smart about cases
        set smartcase

" Highlight search results
        set hlsearch

" Clear the previous search & highlight with :C
        command! C let @/=""
        nnoremap <silent> <C-c> :let @/=""<CR>

" Makes search act like search in modern browsers
        set incsearch

" Don't redraw while executing macros (good performance config)
        set lazyredraw

" For regular expressions turn magic on
        set magic

" Show matching brackets when text indicator is over them
        set showmatch

" How many tenths of a second to blink when matching brackets
        set mat=2

" Add a bit extra margin to the left
        set foldcolumn=5

" Enable folding
        set foldmethod=manual
        set foldlevel=99

" Enable folding with the spacebar
        nnoremap <space> za

" Turn on line numbering
        set nu

" <backspace> key should delete indentation, line ends, characters
        set backspace=indent,eol,start

" Initial status line
        set statusline=%!MyStatusLine()



"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Colors and Fonts
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Enable syntax highlighting
        syntax enable

" Set font according to system
        if has("linux")
            set gfn=IBM\ Plex\ Mono\ 14,:Hack\ 14,Source\ Code\ Pro\ 12,Bitstream\ Vera\ Sans\ Mono\ 11
        elseif has("unix")
            set gfn=Monospace\ 11
        endif

" Enable 256 colors palette in Gnome Terminal
        if $COLORTERM == 'gnome-terminal'
            set t_Co=256
        endif

" Colorscheme
        try
            colorscheme desert
            " colorscheme peaksea
        catch
        endtry

        set background=dark

" Get a highlight in the curren line
        set cursorline



"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Files and backups
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Use Unix as the standard file type
        set ffs=unix,dos,mac

" Turn backup off, since most stuff is in SVN, git etc. anyway...
        set nobackup
        set nowb
        set noswapfile

" Automatically deletes all trailing whitespace and newlines at end of file on save.
        autocmd BufWritePre * %s/\s\+$//e
        autocmd BufWritePre * %s/\n\+\%$//e
        autocmd BufWritePre *.[ch] %s/\%$/\r/e

" Ask for confirmation when quitting a file that has changes
        set confirm

" Turn persistent undo on. Means that you can undo even when you close a buffer/VIM
        try
            set undodir=~/.vim_runtime/temp_dirs/undodir
            set undofile
        catch
        endtry



"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Text, tab and indent related
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Use spaces instead of tabs
        set expandtab

" Be smart when using tabs ;)
        set smarttab

" 1 tab == 4 spaces
        set shiftwidth=4
        set tabstop=4

" Linebreak on 500 characters
        set lbr
        set tw=500

" Auto indent
        set ai

" Smart indent
        set si

" Wrap lines
        set wrap

" Hard wrap at this column
        set textwidth=79

" Make newlines in 'normal' mode, without getting into 'insert' mode
        nnoremap <M-o> o<ESC>
        nnoremap <M-O> O<ESC>

" Text display
        "set listchars=trail:.,tab:>-,extends:>,precedes:<,nbsp:¬
        "set list



"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Visual mode related
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Visual mode pressing * or # searches for the current selection
" Super useful! From an idea by Michael Naumann
        vnoremap <silent> * :<C-u>call VisualSelection('', '')<CR>/<C-R>=@/<CR><CR>
        vnoremap <silent> # :<C-u>call VisualSelection('', '')<CR>?<C-R>=@/<CR><CR>

" Perform dot commands over visual blocks:
        vnoremap . :normal .<CR>


" Turns off highlighting on the bits of code that are changed,
" so the line that is changed is highlighted but the actual text that
" has changed stands out on the line and is readable.
        if &diff
            highlight! link DiffText MatchParen
        endif

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Moving around, tabs and buffers
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Keep cursor in the same column when moving between lines
        set nostartofline

" Buffers contents can be modified
        set modifiable

" Map <space> to / (search) and Ctrl-<space> to ? (backwards search)
        map <leader><space> /
        map <leader><C-space> ?

" Disable highlight when <leader><CR> is pressed
        map <silent> <leader><CR> :noh<CR>

" Splits open at the bottom and right
        set splitbelow splitright

" Shortcutting split navigation, saving a keypress:
        map <C-h> <C-w>h
        map <C-j> <C-w>j
        map <C-k> <C-w>k
        map <C-l> <C-w>l

" Close the current buffer
        map <leader>bd :Bclose<CR>:tabclose<CR>gT

" Close all the buffers
        map <leader>ba :bufdo bd<CR>

" Navigating through buffers
        map <leader>l :bnext<CR>
        map <leader>h :bprevious<CR>

" Useful mappings for managing tabs
        map <leader>tn :tabnew<CR>
        map <leader>to :tabonly<CR>
        map <leader>tc :tabclose<CR>
        map <leader>tm :tabmove
        map <leader>t<leader> :tabnext

" Let 'tl' toggle between this and the last accessed tab
        let g:lasttab = 1
        nmap <Leader>tl :exe "tabn ".g:lasttab<CR>
        au TabLeave * let g:lasttab = tabpagenr()

" Opens a new tab with the current buffer's path
" Super useful when editing files in the same directory
        map <leader>te :tabedit <C-r>=expand("%:p:h")<CR>/

" Switch CWD to the directory of the open buffer
        map <leader>cd :cd %:p:h<CR>:pwd<CR>

" Specify the behavior when switching between buffers
        try
            set switchbuf=useopen,usetab,newtab
            set stal=2
        catch
        endtry

" Return to last edit position when opening files (You want this!)
        au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif

" Retain folds every time I open a file
        autocmd BufWinLeave *.* mkview
        autocmd BufWinEnter *.* silent loadview



"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Status line
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Needed variable to toggle the status bar ON and OFF
        let s:hidden_all = 0
" Toggle the bottom statusbar. Check "Helper functions" header in this file ;)
        nnoremap <leader>h :call ToggleHiddenAll()<CR>



"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Editing mappings
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Move a line of text using ALT+[jk] or Command+[jk] on mac
        nmap <M-j> mz:m+<cr>`z
        nmap <M-k> mz:m-2<cr>`z
        vmap <M-j> :m'>+<cr>`<my`>mzgv`yo`z
        vmap <M-k> :m'<-2<cr>`>my`<mzgv`yo`z



"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Spell checking
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Spell-check set to <leader>se, 'se' for 'spell english':
        map <leader>se :setlocal spell! spelllang=en_us<CR>

" Spell-check set to <leader>ss, 'ss' for 'spell spanish':
        map <leader>ss :setlocal spell! spelllang=es_ar<CR>

" Shortcuts using <leader>
        map <leader>sn ]s
        map <leader>sp [s
        map <leader>sa zg
        map <leader>s? z=



"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Miscellaneous
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Remove the Windows ^M - when the encodings gets messed up
        noremap <Leader>m mmHmt:%s/<C-V><cr>//ge<cr>'tzt'm

" Replace all is aliased to S.
        nnoremap S :%s//g<Left><Left>

" Highlight inside, including, (, ", ', [
        vnoremap $1 <esc>`>a)<esc>`<i(<esc>
        vnoremap $2 <esc>`>a]<esc>`<i[<esc>
        vnoremap $3 <esc>`>a}<esc>`<i{<esc>
        vnoremap $4 <esc>`>a"<esc>`<i"<esc>
        vnoremap $q <esc>`>a'<esc>`<i'<esc>
        vnoremap $e <esc>`>a`<esc>`<i`<esc>

" Map auto complete of (, ", ', [
        inoremap $1 ()<esc>i
        inoremap $2 []<esc>i
        inoremap $3 {}<esc>i
        inoremap $4 {<esc>o}<esc>O
        inoremap $q ''<esc>i
        inoremap $e ""<esc>i

" Fast editing and reloading of vimrc configs
        map <leader>e :e! ~/.config/nvim/init.vim<CR>
        autocmd! bufwritepost ~/.config/nvim/init.vim source ~/.config/nvim/init.vim

" Smart mappings on the command line
        cno $h e ~/
        cno $d e ~/Desktop/
        cno $j e ./
        cno $c e <C-\>eCurrentFileDir("e")<CR>

" $q is super useful when browsing on the command line
" it deletes everything until the last slash
        cno $q <C-\>eDeleteTillSlash()<CR>



"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Helper functions
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Function for toggling the bottom statusbar:
        function! ToggleHiddenAll()
            if s:hidden_all  == 0
                let s:hidden_all = 1
                "set noshowmode
                set noruler
                set laststatus=0
                set noshowcmd
            else
                let s:hidden_all = 0
                set showmode
                "set ruler
                set laststatus=2
                set showcmd
                set statusline=%!MyStatusLine()  " status line
            endif
        endfunction


func! DeleteTillSlash()
    let g:cmd = getcmdline()

    if has("win16") || has("win32")
        let g:cmd_edited = substitute(g:cmd, "\\(.*\[\\\\]\\).*", "\\1", "")
    else
        let g:cmd_edited = substitute(g:cmd, "\\(.*\[/\]\\).*", "\\1", "")
    endif

    if g:cmd == g:cmd_edited
        if has("win16") || has("win32")
            let g:cmd_edited = substitute(g:cmd, "\\(.*\[\\\\\]\\).*\[\\\\\]", "\\1", "")
        else
            let g:cmd_edited = substitute(g:cmd, "\\(.*\[/\]\\).*/", "\\1", "")
        endif
    endif

    return g:cmd_edited
endfunc


func! CurrentFileDir(cmd)
    return a:cmd . " " . expand("%:p:h") . "/"
endfunc


" MyStatusLine()
function! MyStatusLine()
    let statusline = ""
    " Filename (F -> full, f -> relative)
    let statusline .= "%f"
    " Buffer flags
    let statusline .= "%( %h%1*%m%*%r%w%) "
    " File format and type
    let statusline .= "(%{&ff}%(\/%Y%))"
    " Left/right separator
    let statusline .= "%="
    " Line & column
    let statusline .= "(%l,%c%V) "
    " Character under cursor (decimal)
    let statusline .= "%03.3b "
    " Character under cursor (hexadecimal)
    let statusline .= "0x%02.2B "
    " File progress
    let statusline .= "| %P/%L"
    return statusline
endfunction


function! CmdLine(str)
    call feedkeys(":" . a:str)
endfunction


function! VisualSelection(direction, extra_filter) range
    let l:saved_reg = @"
    execute "normal! vgvy"

    let l:pattern = escape(@", "\\/.*'$^~[]")
    let l:pattern = substitute(l:pattern, "\n$", "", "")

    if a:direction == 'gv'
        call CmdLine("Ack '" . l:pattern . "' " )
    elseif a:direction == 'replace'
        call CmdLine("%s" . '/'. l:pattern . '/')
    endif

    let @/ = l:pattern
    let @" = l:saved_reg
endfunction
