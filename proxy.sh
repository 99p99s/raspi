# Set proxy config via profile.d - should apply for all users.
export http_proxy="$PROXY_URL"
export https_proxy="$PROXY_URL"
export ftp_proxy="$PROXY_URL"

# Complete the following variable with the nets that do not
# require the proxy.
export no_proxy="127.0.0.1,localhost,192.168.0.1/8,10.0.0.0/24,*.cnea.gob.ar;*.cnea.gov.ar;www.ib.edu.ar;www2.ib.edu.ar;10.73.0.0/24;172.16.0.0/24;*.cabib;*.local"

# For `curl`.
export HTTP_PROXY="$PROXY_URL"
export HTTPS_PROXY="$PROXY_URL"
export FTP_PROXY="$PROXY_URL"
export NO_PROXY="127.0.0.1,localhost,192.168.0.1/8,10.0.0.0/24,*.cnea.gob.ar;*.cnea.gov.ar;www.ib.edu.ar;www2.ib.edu.ar;10.73.0.0/24;172.16.0.0/24;*.cabib;*.local"
