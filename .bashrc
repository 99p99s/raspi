# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# Set PATH so it includes user's bin if it exists.
#if [ -d "$HOME/bin" ] ; then
#    PATH="$HOME/bin:$PATH"
#fi
[ -d "$HOME/bin" ] && PATH="$HOME/bin:$PATH"

# Set PATH so it includes user's private bin if it exists.
#if [ -d "$HOME/.local/bin" ] ; then
#    PATH="$HOME/.local/bin:$PATH"
#fi
[ -d "$HOME/.local/bin" ] && PATH="$HOME/.local/bin:$PATH"

# I do not need this, but gonna leave it anyway in case it gets
# useful someday.
# If not running interactively, don't do anything.
#case $- in
#    *i*) ;;
#      *) return;;
#esac

# Check if a command exists.
command_exists() {
	command -v "$1" > /dev/null 2>&1
}


# Execute a function with sudo privileges.
#exesudo() {
#    # Params:
#    # ---------------------------------------------------------------------------- #
#    # $1:  string: name of the function to be executed with sudo.
#    #
#    # Usage:
#    # ---------------------------------------------------------------------------- #
#    # exesudo "funcname" followed by any param.
#
#    # Local variables:
#    local _funcname_="$1"          ## Use of underscores to remember it's been passed.
#    local params=( "$@" )              ## Array containing all params passed here.
#    local tmp_file="/tmp/$RANDOM"  ## Temporary file.
#    local file_content             ## Content of the temporary file.
#    local regex                    ## Regular expression.
#    local func                     ## Function source.
#
#    # Shift the first param which is the name of the function.
#    unset params[0]  ## Remove 1st element.
#
#    content="#!/bin/bash\n\n"
#
#    content="${content}params=\n"
#
#    regex="\s+"
#    for param in "${params[@]}"
#    do
#        if [[ "$param" =~ $regex ]]
#            then
#                content="${content}\t\"${param}\"\n"
#            else
#                content="${content}\t${param}\n"
#        fi
#    done
#
#    content="$content\n"
#    echo -e "$content" > "$tmp_file"
#
#    # Append the function source.
#    echo "#$( type "$_funcname_" )" >> "$tmp_file"
#
#    # Append the call to the function.
#    echo -e "\n$_funcname_ \"\${params[@]}\"\n" >> "$tmp_file"
#
#    # Execute the temporary file with sudo.
#    sudo bash "$tmp_file"
#    #rm "$tmp_file"
#    echo $tmp_file
#}

# The following command replaces sudo for my personal
# bash functions. But, use it with care
# as this run a command
# in a subshell with root privileges.
esd() {  # Extended sudo.
    if [[ "$1" = "-"* ]]; then
        sudo "$@"
    elif [ "$(type -t $1)" = "function" ]; then
        local ARGS="$@"; sudo bash -c "$(declare -f $1); $ARGS"
    elif [ "$1" = "command" ] || [ "$1" = "builtin" ]; then
        shift; sudo bash -i <<<"$@"
    else
        sudo bash -i <<<"$@"
    fi
}

# Handle proxy settings
export default_proxy="proxy.cab.cnea.gov.ar:3128"
enable_proxy() {
	if (( $# > 0 )); then
		valid=$(echo $@ | sed -n 's/\([0-9]\{1,3\}.\?\)\{4\}:\([0-9]\+\)/&/p')
		if [[ $valid != $@ ]]; then
			>&2 echo "Invalid address."
			return 1
		fi
		local proxy=$1
		export http_proxy=$proxy \
			https_proxy=$proxy \
			ftp_proxy=$proxy \
			no_proxy="localhost,127.0.0.0/16,::1,10.0.0.0/16,0.0.0.0/16"
		echo "Proxy environment variable set."
		return 0
	else
		echo -n "username -> "; read username
		local pre=""
		if [[ $username != "" ]]; then
			echo -n "password -> "
			read -es password
			pre="$username:$password@"
		fi
		echo -n "server -> "; read server
		if [[ $server != "" ]]; then
			echo -n "port -> "
			read port
			local proxy=$pre$server:$port
			export http_proxy=$proxy \
				https_proxy=$proxy \
				ftp_proxy=$proxy \
				no_proxy="localhost,127.0.0.0/16,::1,10.0.0.0/16,0.0.0.0/16"
			echo "Proxy environment variables set."
			return 0
		fi
		echo "Using default proxy settings."
		export http_proxy=$default_proxy
		export https_proxy=$default_proxy
		export ftp_proxy=$default_proxy
		export no_proxy="localhost,127.0.0.0/16,::1,192.168.1.1/8,10.0.0.0/16,0.0.0.0/16,.cab.cnea.gov.ar,.cnea.gov.ar"
		echo "Proxy environment variable set."
		return 0
	fi
}
disable_proxy() {
	unset http_proxy https_proxy ftp_proxy no_proxy
	echo "Proxy environment variables removed."
}

# Colourise man pages
man() {
	env \
	LESS_TERMCAP_mb=$(tput bold; tput setaf 6) \
	LESS_TERMCAP_md=$(tput bold; tput setaf 6) \
	LESS_TERMCAP_me=$(tput sgr0) \
	LESS_TERMCAP_se=$(tput rmso; tput sgr0) \
	LESS_TERMCAP_ue=$(tput rmul; tput sgr0) \
	LESS_TERMCAP_us=$(tput smul; tput bold; tput setaf 4) \
	LESS_TERMCAP_mr=$(tput rev) \
	LESS_TERMCAP_mh=$(tput dim) \
	LESS_TERMCAP_ZN=$(tput ssubm) \
	LESS_TERMCAP_ZV=$(tput rsubm) \
	LESS_TERMCAP_ZO=$(tput ssupm) \
	LESS_TERMCAP_ZW=$(tput rsupm) \
		man "$@"
}

# Enter directory and list contents
cc() {
	if [ -n "$1" ]; then
		builtin cd "$@" && ls -sAil --color=auto --group-directories-first
	else
		builtin cd ~ && ls -sAil --color=auto --group-directories-first
	fi
}

# Make a directory and immediately enter in it.
md() {
#	if [ -n "$1" ]; then
#		mkdir -p "$@" && cd "$@"
#	fi
	[ -n "$1" ] && mkdir -p "$1" && cd "$1"
}

# Back up a file. Usage "bkp <filename>"
bkp() {
	cp -riv $1 BACKUP-$(date +%Y%m%d%H%M).${1};
}

### ARCHIVE EXTRACTION ###
# usage: unp <file>
upk () {
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2|*.tbz2) tar xjfv $1   ;;
      *.tar.gz|*.tgz)   tar xzfv $1   ;;
      *.bz2)            bunzip2 $1    ;;
      *.rar)            unrar x $1    ;;
      *.gz)             gunzip $1     ;;
      *.tar)            tar xf $1     ;;
      *.zip)            unzip $1      ;;
      *.Z)              uncompress $1 ;;
      *.7z)             7z x $1       ;;
      *)                echo "'$1' cannot be extracted via upk()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}

# Add $USER to a group.
partner() {  # Feed it with the name of a group.
    usermod -a -G $1 $USER
}

set_time() {
    sudo date -s "$(curl -k -4 -s --head https://time.is/Argentina | grep ^date: | sed 's/date: //g')"
    #sudo date -s "$(curl -k -4 -s --head https://www.google.com.ar | grep ^date: | sed 's/date: //g')"
}

# Default pager.  Note that the option I pass to it will quit once you
# try to scroll past the end of the file.
export PAGER="less"
export MANPAGER="$PAGER"
export EDITOR="nvim"

# less/man colors
export LESS=-R
export LESS_TERMCAP_mb="$(printf '%b' '[1;31m')"; a="${a%_}"
export LESS_TERMCAP_md="$(printf '%b' '[1;36m')"; a="${a%_}"
export LESS_TERMCAP_me="$(printf '%b' '[0m')"; a="${a%_}"
export LESS_TERMCAP_so="$(printf '%b' '[01;44;33m')"; a="${a%_}"
export LESS_TERMCAP_se="$(printf '%b' '[0m')"; a="${a%_}"
export LESS_TERMCAP_us="$(printf '%b' '[1;32m')"; a="${a%_}"
export LESS_TERMCAP_ue="$(printf '%b' '[0m')"; a="${a%_}"

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
# Infinite history.
HISTSIZE=-1
HISTFILESIZE=-1

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color|*-256color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
	# We have color support; assume it's compliant with Ecma-48
	# (ISO/IEC-6429). (Lack of such support is extremely rare, and such
	# a case would tend to support setf rather than setaf.)
	color_prompt=yes
    else
	color_prompt=
    fi
fi

if [ "$color_prompt" = yes ]; then
    #PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w \$\[\033[00m\] '
    PS1='${debian_chroot:+($debian_chroot)}\n\[\e[31;1m\](\[\e[36;1m\]\h\[\e[31;1m\])-(\[\e[34;1m\]\u\[\e[31;1m\])-(\[\e[37;1m\]\w\[\e[31;1m\])\n\[\e[31;1m\](\[\e[33;1m\]\!\[\e[31;1m\]) \[\e[0;34;1m\]\$\[\033[00m\] '
    PS2='\[\e[0;31;1m\]=\[\e[0;32;1m\]=\[\e[0;33;1m\]=\[\e[0;34;1m\]=\[\e[0;35;1m\]>\[\033[00m\] '
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

# colored GCC warnings and errors
export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# The first alias I always define.
alias l='ls -sAil --color=auto --group-directories-first'

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.
if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# Load shortcuts & aliases
[ -f "$HOME/.config/shortcutsrc" ] && source "$HOME/.config/shortcutsrc"
[ -f "$HOME/.config/aliasrc" ] && source "$HOME/.config/aliasrc"

# I guess this does what it says.
shopt -s expand_aliases

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

# Enable tab completion when starting a command with 'sudo'
[ "$PS1" ] && complete -cf sudo

# Disable ctrl-s and ctrl-q.
stty -ixon

# ´cd´ into directory merely by typing the directory name.
shopt -s autocd

# When I need to copy the contents of a file to the clipboard
if command_exists xclip; then
	alias xclipc='xclip -selection clipboard' # followed by path to file
fi

[[ -s /home/control/.autojump/etc/profile.d/autojump.sh ]] && source /home/control/.autojump/etc/profile.d/autojump.sh

export PATH="$HOME/neovim/bin:$PATH"
export PATH="$HOME/Arduino/bin:$PATH"
export PYENV_ROOT="$HOME/.pyenv"
export PATH="$PYENV_ROOT/bin:$PATH"
if command -v pyenv 1>/dev/null 2>&1; then
 eval "$(pyenv init --path)"
fi
eval "$(pyenv virtualenv-init -)"
export DOTNET_ROOT=/opt/dotnet

set_time
