# Move 80proxy to somewhere else.
sudo mv /etc/apt/apt.conf.d/80proxy $HOME/.config/

# Move proxy.sh to somewhere else.
sudo mv /etc/profile.d/proxy.sh $HOME/.config/

# Modify ´environment´: comment proxy variables.
sudo sed -e '/proxy/ s/^#*/#/' -i /etc/environment
sudo sed -i '/^[^#]/ s/\(^.*PROXY.*$\)/#\1/' /etc/environment
