# Move 80proxy to somewhere else.
sudo mv $HOME/.config/80proxy /etc/apt/apt.conf.d/

# Move proxy.sh to somewhere else.
sudo mv $HOME/.config/proxy.sh /etc/profile.d/

# Modify ´environment´: uncomment proxy variables.
#sudo sed -e '/proxy/ s/^#*/#/' -i /etc/environment
#sudo sed -i '/^[^#]/ s/\(^.*PROXY.*$\)/#\1/' /etc/environment
sudo sed -i -E 's/^#(export (http|HTTP|ftp|FTP|no_|NO_|PROXY))/\1/g' /etc/environment
